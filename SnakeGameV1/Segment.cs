﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGameV1
{
    class Segment : IEquatable<Segment>
    {

        int x; int y;
        int direction;
        public Segment(int x, int y, int direction) { this.x = x; this.y = y; this.direction = direction; }

        public int getX() { return x; }
        public int getY() { return y; }
        public int getDir() { return direction; }

        public void setX(int X) { this.x = X; }
        public void setY(int Y) { this.y = Y; }
        public void setDir(int dir) { this.direction = dir; }

        public bool Equals(Segment other)
        {
            if (this.x == other.x && this.y == other.y) return true;
            else return false;
        }
    }
}
