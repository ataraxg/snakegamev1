﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGameV1
{
    public partial class MainBoard : Form
    {
        public const int LEFT = 0;
        public const int RIGHT = 1;
        public const int UP = 2;
        public const int DOWN = 3;

        private int time = 0;
        private bool running = false;
        private int direction = 0;

        private int direction_2 = 0;

        Random rnd = new Random();

        PictureBox[,] pictureBox1 = new PictureBox[38, 20];
        PictureBox snake = new PictureBox();

        List<Coordinate> listFruit = new List<Coordinate>();
        List<Coordinate> listWall = new List<Coordinate>();
        List<Segment> listSnake = new List<Segment>();

        List<Segment> listSnake2 = new List<Segment>();

        private string pseudo;

        public MainBoard(string strpseudo)
        {
            InitializeComponent();
            KeyDown += new KeyEventHandler(MainForm_KeyDown); // key event
            this.pseudo = strpseudo;
            this.PseudoLabel.Text = pseudo;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetFeatureToAllControls(this.Controls); // keyboard control

            //
            // Field inititalisation
            //
            int x = 0;
            int y = 0;
            for (int i = 0; i < 38; i++)
            {
                y = 0;
                for (int j = 0; j < 20; j++) {

                    pictureBox1[i, j] = new PictureBox();
                    pictureBox1[i, j].Location = new System.Drawing.Point(x, y);
                    pictureBox1[i, j].ImageLocation = @"Images\grass-background.jpg";
                    pictureBox1[i, j].Size = new System.Drawing.Size(20, 20);

                    y = y + 20;

                    panel1.Controls.Add(pictureBox1[i, j]);
                }
                x = x + 20;
            }
            //
            // Snake inititalisation
            //
            int snakeX = 17;
            int snakeY = 10;

            pictureBox1[snakeX, snakeY].ImageLocation = @"Images\headLeft.png";
            listSnake.Add(new Segment(snakeX, snakeY, direction));

            snakeX = 5;
            snakeY = 17;
            pictureBox1[snakeX, snakeY].ImageLocation = @"Images\headLeft.png";
            listSnake2.Add(new Segment(snakeX, snakeY, direction_2));

            //
            // Fruit / Wall
            //
            addElement();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (running == false)
            {
                button.Text = "Stop";
                timer1.Enabled = true;
                timer1.Interval = 100;
                timer1.Start();
                running = true;
            }
            else {
                button.Text = "Start";
                timer1.Stop();
                running = false;
            }
        }

        private void addElement() 
            // 
            // Add a new fruit and a wall
            //
        {
            int randX;
            int randY;

            do
            {
                randX = rnd.Next(0, 37);
                randY = rnd.Next(0, 19);
            } while (CheckTable(randX,randY));

            pictureBox1[randX, randY].ImageLocation = @"Images\cherry.png";
            listFruit.Add(new Coordinate(randX, randY));

            do
            {
                randX = rnd.Next(0, 37);
                randY = rnd.Next(0, 19);
            } while (CheckTable(randX, randY));

            pictureBox1[randX, randY].ImageLocation = @"Images\wall.png";
            listWall.Add(new Coordinate(randX, randY));
        }

        private bool CheckTable(int randX, int randY)
            //
            // Check if new coordinates are not already taken
            //
        {
            if (listFruit.Contains(new Coordinate(randX, randY)) || listWall.Contains(new Coordinate(randX, randY)) || listSnake.Contains(new Segment(randX, randY, 0)) || listSnake2.Contains(new Segment(randX, randY, 0))) return true;
            else return false;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left && listSnake.First().getDir() != RIGHT)
            {
                direction = LEFT;
                DirLabel.Text = "Left";
            }
            if (e.KeyCode == Keys.Q && listSnake2.First().getDir() != RIGHT)
            {
                direction_2 = LEFT;
            }
            else if (e.KeyCode == Keys.Right && listSnake.First().getDir() != LEFT)
            {
                direction = RIGHT;
                DirLabel.Text = "Right";
            }
            else if (e.KeyCode == Keys.D && listSnake2.First().getDir() != LEFT)
            {
                direction_2 = RIGHT;
            }
            else if (e.KeyCode == Keys.Up && listSnake.First().getDir() != DOWN)
            {
                direction = UP;
                DirLabel.Text = "Up";
            }
            else if (e.KeyCode == Keys.Z && listSnake2.First().getDir() != DOWN)
            {
                direction_2 = UP;
            }
            else if (e.KeyCode == Keys.Down && listSnake.First().getDir() != UP)
            {
                direction = DOWN;
                DirLabel.Text = "Down";
            }
            else if (e.KeyCode == Keys.S && listSnake2.First().getDir() != UP)
            {
                direction_2 = DOWN;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
            //
            // Game loop at each tick of the clock
            //
        {

            Segment last = listSnake.Last();
            Segment first = listSnake.First();

            Segment last2 = listSnake2.Last();
            Segment first2 = listSnake2.First();

            Coordinate nextPos = null;

            listSnake.First().setDir(direction);

            listSnake2.First().setDir(direction_2);
            Coordinate nextPos2 = null;

            if (direction_2 == LEFT)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos2 = new Coordinate(first2.getX() - 1, first2.getY());
            }
            else if (direction_2 == RIGHT)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos2 = new Coordinate(first2.getX() + 1, first2.getY());

            }
            else if (direction_2 == UP)
            {
                time++;
                TimerLabel.Text = time.ToString();
                nextPos2 = new Coordinate(first2.getX(), first2.getY() - 1);
            }
            else if (direction_2 == DOWN)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos2 = new Coordinate(first2.getX(), first2.getY() + 1);
                    
            }

            if (direction == LEFT)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos = new Coordinate(first.getX() - 1, first.getY());
            }
            else if (direction == RIGHT)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos = new Coordinate(first.getX() + 1, first.getY());

            }
            else if (direction == UP)
            {
                time++;
                TimerLabel.Text = time.ToString();
                nextPos = new Coordinate(first.getX(), first.getY() - 1);
            }
            else if (direction == DOWN)
            {
                time++;
                TimerLabel.Text = time.ToString();

                nextPos = new Coordinate(first.getX(), first.getY() + 1);

            }

            Segment s1 = new Segment(nextPos.x, nextPos.y, 0);
            Segment s2 = new Segment(nextPos2.x, nextPos2.y, 0);


            if (listWall.Contains(nextPos)||listWall.Contains(nextPos2))
            {
                timer1.Stop();
                running = false;
            }
            else if(listSnake.Contains(s1)||listSnake2.Contains(s2) || listSnake.Contains(s2) || listSnake2.Contains(s1))
            {
                timer1.Stop();
                running = false;
            }
            else {
                pictureBox1[last.getX(), last.getY()].ImageLocation = @"Images\grass-background.jpg";
                pictureBox1[last2.getX(), last2.getY()].ImageLocation = @"Images\grass-background.jpg";

                if (listFruit.Contains(nextPos))
                {
                    addSegment(last,1);
                    listFruit.Remove(nextPos);
                    addElement();
                }else if (listFruit.Contains(nextPos2))
                {
                    addSegment(last2, 2);
                    listFruit.Remove(nextPos2);
                    addElement();
                }

                int Position;
                int nextDir = direction;
                int oldDir;

                foreach (Segment s in listSnake)
                {
                    switch (s.getDir())
                    {
                        case LEFT:
                            Position = s.getX();
                            if (Position == 0) Position = 38;
                            s.setX((Position - 1));
                            break;
                        case RIGHT:
                            Position = s.getX();
                            if (Position == 37) Position = -1;
                            s.setX((Position + 1));
                            break;
                        case UP:
                            Position = s.getY();
                            if (Position == 0) Position = 20;
                            s.setY((Position - 1));
                            break;
                        case DOWN:
                            Position = s.getY();
                            if (Position == 19) Position = -1;
                            s.setY((Position + 1));
                            break;
                    }
                    oldDir = s.getDir();
                    s.setDir(nextDir);
                    nextDir = oldDir;

                    pictureBox1[s.getX(), s.getY()].ImageLocation = @"Images\snake.jpg";
                }

                nextDir = direction_2;

                foreach (Segment s in listSnake2)
                {
                    switch (s.getDir())
                    {
                        case LEFT:
                            Position = s.getX();
                            if (Position == 0) Position = 38;
                            s.setX((Position - 1));
                            break;
                        case RIGHT:
                            Position = s.getX();
                            if (Position == 37) Position = -1;
                            s.setX((Position + 1));
                            break;
                        case UP:
                            Position = s.getY();
                            if (Position == 0) Position = 20;
                            s.setY((Position - 1));
                            break;
                        case DOWN:
                            Position = s.getY();
                            if (Position == 19) Position = -1;
                            s.setY((Position + 1));
                            break;
                    }
                    oldDir = s.getDir();
                    s.setDir(nextDir);
                    nextDir = oldDir;

                    pictureBox1[s.getX(), s.getY()].ImageLocation = @"Images\snake.jpg";
                }
            }

            if(direction == LEFT) pictureBox1[first.getX(), first.getY()].ImageLocation = @"Images\headLeft.png";
            if (direction == RIGHT) pictureBox1[first.getX(), first.getY()].ImageLocation = @"Images\headRight.png";
            if (direction == UP) pictureBox1[first.getX(), first.getY()].ImageLocation = @"Images\headUp.png";
            if (direction == DOWN) pictureBox1[first.getX(), first.getY()].ImageLocation = @"Images\headDown.png";

            if (direction_2 == LEFT) pictureBox1[first2.getX(), first2.getY()].ImageLocation = @"Images\headLeft.png";
            if (direction_2 == RIGHT) pictureBox1[first2.getX(), first2.getY()].ImageLocation = @"Images\headRight.png";
            if (direction_2 == UP) pictureBox1[first2.getX(), first2.getY()].ImageLocation = @"Images\headUp.png";
            if (direction_2 == DOWN) pictureBox1[first2.getX(), first2.getY()].ImageLocation = @"Images\headDown.png";


        }

        private void addSegment(Segment last, int Snake)
            //
            // Add a new segment to the snake
            //
        {
            if(Snake == 1)
            {
                switch (last.getDir())
                {
                    case LEFT:
                        listSnake.Add(new Segment(last.getX() + 1, last.getY(), last.getDir()));
                        break;
                    case RIGHT:
                        listSnake.Add(new Segment(last.getX() - 1, last.getY(), last.getDir()));
                        break;
                    case UP:
                        listSnake.Add(new Segment(last.getX(), last.getY() + 1, last.getDir()));
                        break;
                    case DOWN:
                        listSnake.Add(new Segment(last.getX(), last.getY() - 1, last.getDir()));
                        break;
                }
            }else if (Snake == 2)
            {
                switch (last.getDir())
                {
                    case LEFT:
                        listSnake2.Add(new Segment(last.getX() + 1, last.getY(), last.getDir()));
                        break;
                    case RIGHT:
                        listSnake2.Add(new Segment(last.getX() - 1, last.getY(), last.getDir()));
                        break;
                    case UP:
                        listSnake2.Add(new Segment(last.getX(), last.getY() + 1, last.getDir()));
                        break;
                    case DOWN:
                        listSnake2.Add(new Segment(last.getX(), last.getY() - 1, last.getDir()));
                        break;
                }
            }
        }

        private void SetFeatureToAllControls(Control.ControlCollection cc)
        {
            if (cc != null)
            {
                foreach (Control control in cc)
                {
                    control.PreviewKeyDown += new PreviewKeyDownEventHandler(control_PreviewKeyDown);
                    SetFeatureToAllControls(control.Controls);
                }
            }
        }

        void control_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Z || e.KeyCode == Keys.S || e.KeyCode == Keys.Q || e.KeyCode == Keys.D)
            {
                e.IsInputKey = true;
            }
        }
    }
}
