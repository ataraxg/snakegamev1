﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGameV1
{
    public partial class Menu : Form
    {
        private TcpClient _client;
        Boolean _isConnected;
        string readData = null;
        private List<string> playersList = null;

        public Menu()
        {
            InitializeComponent();
            
        }

        public void HandleCommunication()
        {
            _isConnected = true;
            
            while (_isConnected)
            {
                try
                {
                    NetworkStream ns = _client.GetStream();
                    IFormatter formatter = new BinaryFormatter();
                    playersList = (List<string>)formatter.Deserialize(ns);
                    //MessageBox.Show("Liste recue de la part du serveur");
                    
                }
                catch(Exception ex)
                {

                }
            }
        }

        public void sendData(string playerId)
        {
            if (!_client.Connected)
            {
               
                _client = new TcpClient();
                _client.Connect("127.0.0.1", 8888);
            }
            /*BinaryFormatter formatter = new BinaryFormatter();
            NetworkStream ns = _client.GetStream();
            formatter.Serialize(ns, playerId);*/
            NetworkStream ns = _client.GetStream();
            byte[] outputStream = Encoding.ASCII.GetBytes(pseudoBox.Text);
            ns.Write(outputStream, 0, outputStream.Length);
            ns.Flush();
            //MessageBox.Show("Id envoyée au serveur");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _client = new TcpClient();
                _client.Connect("127.0.0.1", 8888);
                Thread HandleCom = new Thread(HandleCommunication);
                HandleCom.Start();
                sendData(pseudoBox.Text);
                SousMenu sousMenu = new SousMenu(pseudoBox.Text, playersList);
                this.Hide();
                sousMenu.ShowDialog();
                this.Show();
            } catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au serveur. Vous allez être redirigé vers le jeu solo_");
                MainBoard mainBoard = new MainBoard(pseudoBox.Text);
                this.Hide();
                mainBoard.ShowDialog();
                this.Show();
            }
        }

        
    }
}
