﻿namespace SnakeGameV1
{
    partial class SousMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.offlineMode = new System.Windows.Forms.Button();
            this.playerList = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.onlineMode = new System.Windows.Forms.Button();
            this.playerId = new System.Windows.Forms.Label();
            this.state = new System.Windows.Forms.Label();
            this.playerState = new System.Windows.Forms.Label();
            this.Quit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // offlineMode
            // 
            this.offlineMode.Location = new System.Drawing.Point(209, 122);
            this.offlineMode.Name = "offlineMode";
            this.offlineMode.Size = new System.Drawing.Size(150, 23);
            this.offlineMode.TabIndex = 0;
            this.offlineMode.Text = "Jouer en solo";
            this.offlineMode.UseVisualStyleBackColor = true;
            this.offlineMode.Click += new System.EventHandler(this.offlineMode_Click);
            // 
            // playerList
            // 
            this.playerList.Location = new System.Drawing.Point(12, 101);
            this.playerList.Name = "playerList";
            this.playerList.Size = new System.Drawing.Size(139, 215);
            this.playerList.TabIndex = 1;
            this.playerList.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Liste des joueurs connectés";
            // 
            // onlineMode
            // 
            this.onlineMode.Location = new System.Drawing.Point(209, 192);
            this.onlineMode.Name = "onlineMode";
            this.onlineMode.Size = new System.Drawing.Size(150, 23);
            this.onlineMode.TabIndex = 3;
            this.onlineMode.Text = "Jouer en ligne";
            this.onlineMode.UseVisualStyleBackColor = true;
            // 
            // playerId
            // 
            this.playerId.AutoSize = true;
            this.playerId.Location = new System.Drawing.Point(15, 26);
            this.playerId.Name = "playerId";
            this.playerId.Size = new System.Drawing.Size(42, 13);
            this.playerId.TabIndex = 4;
            this.playerId.Text = "pseudo";
            // 
            // state
            // 
            this.state.AutoSize = true;
            this.state.Location = new System.Drawing.Point(15, 43);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(25, 13);
            this.state.TabIndex = 5;
            this.state.Text = "état";
            // 
            // playerState
            // 
            this.playerState.AutoSize = true;
            this.playerState.Location = new System.Drawing.Point(88, 43);
            this.playerState.Name = "playerState";
            this.playerState.Size = new System.Drawing.Size(53, 13);
            this.playerState.TabIndex = 6;
            this.playerState.Text = "Connecté";
            // 
            // Quit
            // 
            this.Quit.Location = new System.Drawing.Point(209, 251);
            this.Quit.Name = "Quit";
            this.Quit.Size = new System.Drawing.Size(150, 23);
            this.Quit.TabIndex = 7;
            this.Quit.Text = "Quitter";
            this.Quit.UseVisualStyleBackColor = true;
            this.Quit.Click += new System.EventHandler(this.Quit_Click);
            // 
            // SousMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 355);
            this.Controls.Add(this.Quit);
            this.Controls.Add(this.playerState);
            this.Controls.Add(this.state);
            this.Controls.Add(this.playerId);
            this.Controls.Add(this.onlineMode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.playerList);
            this.Controls.Add(this.offlineMode);
            this.Name = "SousMenu";
            this.Text = "SousMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button offlineMode;
        private System.Windows.Forms.ListView playerList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button onlineMode;
        private System.Windows.Forms.Label playerId;
        private System.Windows.Forms.Label state;
        private System.Windows.Forms.Label playerState;
        private System.Windows.Forms.Button Quit;
    }
}