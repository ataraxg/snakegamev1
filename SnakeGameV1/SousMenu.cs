﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeGameV1
{

    public partial class SousMenu : Form
    {

        public delegate void uploadListe(List<string> list);

        private TcpClient _client;
        Boolean _isConnected;
        private string pseudo;
        private List<string> playersList = new List<string>();

        public SousMenu(string strpseudo, List<string> pList)
        {

            InitializeComponent();
            this.pseudo = strpseudo;
            this.playerId.Text = pseudo;
            this.playersList = pList;
            if (playersList == null) playerList.Items.Add("Pas de joueurs connectés");
            else {
                foreach (string playerId in playersList)
                {
                    playerList.Items.Add(playerId);
                }
            }
            _client = new TcpClient();
            _client.Connect("127.0.0.1", 8888);
            Thread HandleCom = new Thread(HandleCommunication);
            HandleCom.Start();

        }
        public void HandleCommunication()
        {    
            _isConnected = true;
            while (_isConnected)
            {
                    try
                    {
                    NetworkStream ns = _client.GetStream();
                    IFormatter formatter = new BinaryFormatter();
                    playersList = (List<string>)formatter.Deserialize(ns);
                    MessageBox.Show("Liste recue de la part du serveur");
                    Invoke((uploadListe)upload, playersList);
                    playerList.EnsureVisible(playerList.Items.Count - 1);
                    Application.DoEvents();
                    }
                    catch (Exception ex)
                    {

                    }
          
            }
            
        }

        public void upload(List<string> test)
        {
            playerList.Items.Clear();
            if (playersList == null) playerList.Items.Add("Pas de joueurs connectés");
            else {
                foreach (string playerId in playersList)
                {
                    playerList.Items.Add(playerId);
                }
            }
        }

        private void offlineMode_Click(object sender, EventArgs e)
        {
            MainBoard mainboard = new MainBoard(playerId.Text);
            this.Hide();
            mainboard.ShowDialog();
            this.Show();
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            NetworkStream ns = _client.GetStream();
            byte[] outputStream = Encoding.ASCII.GetBytes("$"+playerId.Text);
            ns.Write(outputStream, 0, outputStream.Length);
            ns.Flush();
            this.Close();
        }
    }
}
