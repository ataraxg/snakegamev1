﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeGameV1
{
    class Coordinate : IEquatable<Coordinate>
    {
        public int x { get; set; }
        public int y { get; set; }

        public Coordinate(int x, int y) { this.x = x; this.y = y; }

        public bool Equals(Coordinate other)
        {
            if (this.x == other.x && this.y == other.y) return true;
            else return false;
        }
    }
}
