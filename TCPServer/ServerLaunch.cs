﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TCPServer
{
    class ServerLaunch
    {
        TcpListener _server;
        Boolean _isRunning;

        private List<TcpClient> ConnectedClients = new List<TcpClient>();
        private List<String> playersList = new List<string>();

         static void Main()
        {
            ServerLaunch server = new ServerLaunch();
            server.TcpServer();
            
        }

        public void TcpServer()
        {
            IPAddress ipAd = IPAddress.Parse("127.0.0.1");
            _server = new TcpListener(ipAd, 8888);
            _server.Start();
            Console.WriteLine("Le serveur est en route");
            _isRunning = true;
            LoopClients();
        }

        public void LoopClients()
        {
            while (_isRunning)
            {
                //On attend un connexion client
                //on ajoute le client à la liste des clients connectés
                TcpClient newClient = _server.AcceptTcpClient();
                ConnectedClients.Add(newClient);

                //Creation d'un thread  pour gérer la communication client/serveur
                Thread myThread = new Thread(new ParameterizedThreadStart(HandleClient));
                myThread.Start(newClient);
            }
        }

        public void HandleClient(object obj)
        {
            //On récupère le client passé en paramètre du thread
            TcpClient client = (TcpClient)obj;
            //on crée deux streams
            StreamReader sReader = new StreamReader(client.GetStream(), Encoding.ASCII);
            StreamWriter sWriter = new StreamWriter(client.GetStream(), Encoding.ASCII);
            Boolean bClientConnected = true;
            while (bClientConnected)
            {
                try
                {
                    //On récupère notre objet du client
                    
                    if (!client.Connected) return;
                    NetworkStream ns = client.GetStream();
                    string dataFromClient = null;
                    byte[] bytesFrom = new byte[100000];
                    ns.Read(bytesFrom, 0, client.ReceiveBufferSize);
                    int i = bytesFrom.Length - 1;
                    while (bytesFrom[i] == 0)
                    {
                        --i;
                    }
                    byte[] bytesFromShort = new byte[i + 1];
                    Array.Copy(bytesFrom, bytesFromShort, i + 1);
                    dataFromClient = Encoding.ASCII.GetString(bytesFromShort);
                    //Console.WriteLine("Id recue de la part du client : "+dataFromClient);
                    if (dataFromClient.StartsWith("$")) playersList.Remove(dataFromClient.Substring(1));
                    else playersList.Add(dataFromClient);
                    //On l'envoie à tout les clients connectés
                    foreach (TcpClient t in ConnectedClients)
                    {
                        try
                        {
                            TcpClient tt = t;
                            IFormatter formatter1 = new BinaryFormatter();
                            NetworkStream ns1 = tt.GetStream();
                            formatter1.Serialize(ns1, playersList);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    Console.WriteLine("Envoi de la liste à tout les clients : fait!");
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
            